perc_GC = ((4500 + 2575)/14800) * 100
print(f"Le pourcentage de GC est {perc_GC:.0f} %") # ou encore print(f"Le pourcentage de GC est {perc_GC:d} %") 
print("Le pourcentage de GC est {:.1f} %".format(perc_GC))
print(f"Le pourcentage de GC est {perc_GC:.2f} %")
print(f"Le pourcentage de GC est {perc_GC:.3f} %")